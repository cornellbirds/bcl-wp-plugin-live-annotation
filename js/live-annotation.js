

jQuery(document).ready(function () {

    // Field Guide and Tutorial Links - open content and smooth scroll
    jQuery('a.learn-button').click(function (event) {
        var target = jQuery(this.hash),
            guideVideos = document.querySelectorAll('#annotation-field-guide video');
        target = target.length ? target : jQuery('[name=' + this.hash.slice(1) + ']');
        // If this is the field guide, start videos
        if (target == "#annotation-field-guide") {
            guideVideos.forEach(function() {
                jQuery(this).play();
            });
        }
        jQuery('.learn-container').removeClass('show');
        target.addClass('show');
        scrollToTarget(this);
    });

    // Smooth scroll to content
    jQuery('a.learn-back').click(function () {
        scrollToTarget(this);
    });

    // Smooth scroll function
    function scrollToTarget(that) {
        var target = jQuery(that.hash);
        target = target.length ? target : jQuery('[name=' + that.hash.slice(1) + ']');
        if (target.length) {
            event.preventDefault();
            jQuery('html, body').animate({
                scrollTop: target.offset().top
            }, 1000, function () {
                // Change focus
                var $target = jQuery(target);
                $target.focus();
                if ($target.is(":focus")) {
                    return false;
                } else {
                    $target.attr('tabindex', '-1');
                    $target.focus();
                }
            });
        }
    } // scrollToTarget

    // Change video to sticky when it is out of view port
    var annotationContainer = jQuery('#annotation-container'),
        videoContainer = annotationContainer.find('.bp-live_annotation-video-container');
    jQuery(window).on('scroll', function () {
        if ((!annotationContainer.isInViewport()) && jQuery('.bp-live_annotation-content.annotate').hasClass('show')) {
            videoContainer.addClass('sticky');
        } else {
            videoContainer.removeClass('sticky');
        }
    });

    // In viewport function
    jQuery.fn.isInViewport = function () {
        var elementTop = jQuery(this).offset().top;
        var elementBottom = elementTop + jQuery(this).outerHeight() - 50;

        var viewportTop = jQuery(window).scrollTop();
        var viewportBottom = viewportTop + jQuery(window).height();

        return elementBottom > viewportTop && elementTop < viewportBottom;
    };

});

CLO_LIVE_ANNOTATION = (function () {

    // A note about the ajax calls.
    // per https://developer.wordpress.org/rest-api/using-the-rest-api/authentication/, we must include the nonce for the request in the
    // header. Note that this technique will only work as long as all the other word press cookies are in place (they get sent
    // in the ajax call) (and of course the user must be logged in).
    // I verified that this works by keeping the annotation post up, but then logging the current user out on another page.
    // In this case, the server correctly gives an unauthorized response (it says the nonce has expired).
    let setUp = function (configOptions) {
        
        startAnnotation = false;

        jQuery('#' + configOptions.annotationStopButtonContainerId).hide();

        let jqElemStartButton = jQuery(`       <button class="btn btn-primary btn-lg btn-block start-session ${configOptions.annotationButtonClassPost}"
                                                type="button"
                                                name="start"
                                                value="start"
                                                id="start-session">
                                                START
                                            </button>`);

        jqElemStartButton.click(function () {


                // The user is starting to annotate, so now we build the buttons from meta data
                jQuery.ajax({
                    url: configOptions.metaDataUrl + configOptions.postId,
                    dataType: 'json',
                    type: 'get',
                    beforeSend: function (jqXhr) {
                        jqXhr.setRequestHeader('X-WP-Nonce', configOptions.nonce);
                    },
                    contentType: 'application/json',
                    success: function (data, textStatus, jQxhr) {

                        if (startAnnotation === false) {
                            startAnnotation = true;

                            data.forEach(
                                buttonMeta => {

                                    let jqElemImage = buttonMeta.image_file_name ? `${buttonMeta.image_file_name}` : "";

                                    let jqElemButtonContainer = jQuery(`
                                            <div class="button ${buttonMeta.data_attribute_name}">
                                                
                                            </div>`);

                                    // let jqElemCurrentAnnotationButton = jQuery(`
                                    //         <button class="${configOptions.annotationButtonClassPost}"
                                    //             type="button"
                                    //             name="${buttonMeta.data_attribute_name}"
                                    //             value="${buttonMeta.data_attribute_value}"
                                    //             style="background-image:url(${jqElemImage});">
                                    //             ${buttonMeta.label}
                                    //         </button>`);

                                    let jqElemCurrentAnnotationButton = jQuery(`
                                    <button class="${configOptions.annotationButtonClassPost}"
                                        type="button"
                                        name="${buttonMeta.data_attribute_name}"
                                        value="${buttonMeta.data_attribute_value}">
                                        <span>${buttonMeta.label}</span>
                                        <img src="${jqElemImage}" />
                                    </button>`);

                                    // set up the ajax call for the click event to add the annotation
                                    jqElemCurrentAnnotationButton.click(function () {
                                        jQuery.ajax({
                                            url: configOptions.annotationUrl,
                                            dataType: 'json',
                                            type: 'post',
                                            beforeSend: function (jqXhr) {
                                                jqXhr.setRequestHeader('X-WP-Nonce', configOptions.nonce);
                                            },
                                            contentType: 'application/json',
                                            data: JSON.stringify({
                                                "post_id": configOptions.postId,
                                                "observation_name": buttonMeta.data_attribute_name,
                                                "observation_value": buttonMeta.data_attribute_value
                                            }),
                                            success: function (data, textStatus, jQxhr) {
                                                console.log("Success! " + JSON.stringify(data, null, 2));

                                                // having added the annotation, we now write the event status
                                                // and give the user the opportunity to soft-delete the annotation
                                                let jqElemRemoveButton = jQuery(`<button 
                                                    id="${data.id}"
                                                    class="${configOptions.annotationButtonClassDelete}"
                                                    type="button"
                                                    name="${data.observation_name}"
                                                    value="${data.observation_value}">
                                                </button>`);

                                                // Create/refresh scrollbar for status area
                                                jQuery('.nano').nanoScroller();

                                                jqElemRemoveButton.click(function () {
                                                    jQuery.ajax({
                                                        url: configOptions.annotationUrl + data.id + '/',
                                                        dataType: 'json',
                                                        type: 'delete',
                                                        beforeSend: function (jqXhr) {
                                                            jqXhr.setRequestHeader('X-WP-Nonce', configOptions.nonce);
                                                        },
                                                        contentType: 'application/json',
                                                        success: function (data, textStatus, jQxhr) {
                                                            console.log("Success! " + JSON.stringify(data, null, 2));

                                                            // Change annotation text in status then remove
                                                            var jqElemRemoved = jQuery("#" + data.id).parent('p');
                                                            jqElemRemoved.addClass('remove').html('<em>Observation removed</em>');
                                                            window.setTimeout(function () {
                                                                jqElemRemoved.remove();
                                                                jQuery('.nano').nanoScroller();
                                                            }, 2000);

                                                        }, // on success for add annotation
                                                        error: function (jqXhr, textStatus, errorThrown) {
                                                            console.log("Error " + errorThrown + ", " + textStatus + ", " + jqXhr.responseText);
                                                        }
                                                    }) // ajax call for click remove button
                                                });

                                                // Add annotation to status area
                                                let jQueryRemoveButtonParagraph = jQuery(`<p class="new">${(new Date).toLocaleTimeString()}: ${data.observation_value}</p>`)
                                                    .append(jqElemRemoveButton);

                                                // Timeout for "new" class on annotation
                                                window.setTimeout(function () {
                                                    jQueryRemoveButtonParagraph.removeClass('new')
                                                }, 2000);


                                                jQuery("#" + configOptions.statusAreaContainerId)
                                                    .prepend(jQuery(jQueryRemoveButtonParagraph));

                                            }, // on success for add annotation
                                            error: function (jqXhr, textStatus, errorThrown) {
                                                console.log("Error " + errorThrown + ", " + textStatus + ", " + jqXhr.responseText);
                                            }
                                        }); // ajax call for click to add annotation

                                        // Interface - button active state
                                        jqElemCurrentAnnotationButton.parent('.button').addClass('on');
                                        window.setTimeout(function () {
                                            jqElemCurrentAnnotationButton.parent('.button').removeClass('on')
                                        }, 2000);

                                    });

                                    jqElemButtonContainer.append(jqElemCurrentAnnotationButton);

                                    jQuery("#" + configOptions.annotationButtonAddContainerId)
                                        .append(jQuery(jqElemButtonContainer));

                                }); // for each on meta data
                            }

                        // Display video feed
                        let jqElemVideoIframe = jQuery(
                            `<iframe width="560" height="315"
                                src="${configOptions.feedUrl}?rel=0&autoplay=true&disablekb=1&controls=0&modestbranding=1"
                                allow="autoplay; encrypted-media"
                                controls="0"
                                showinfo="0"
                                class="fleft"
                                width="696"
                                height="392"
                                frameborder="0"
                                id="yt-player">                          
                              </iframe>`);
                        if (jQuery('#' + configOptions.annotationVideoContainerId).hasClass('hidden')) {
                            jQuery('#' + configOptions.annotationVideoContainerId)
                                .append(jqElemVideoIframe)
                                .removeClass('hidden');
                            jQuery('#bp-intro-text').addClass('hidden');
                        }

                        // set button state
                        jQuery('#' + configOptions.annotationStartButtonContainerId).hide();
                        jQuery('#' + configOptions.annotationStopButtonContainerId).show();

                        // Clear status
                        jQuery("#" + configOptions.statusAreaContainerId).empty();

                        // Interface session toggle - hide welcome, show annotation
                        jQuery(".nano").nanoScroller({stop: true});
                        jQuery('div[data-session="welcome"]').removeClass('show');
                        jQuery('div[data-session="annotate"]').addClass('show');
                        jQuery('.status h4 span').hide();

                        // Having created the buttons, now we send the start session annotation
                        jQuery.ajax({
                            url: configOptions.annotationUrl,
                            dataType: 'json',
                            type: 'post',
                            beforeSend: function (jqXhr) {
                                jqXhr.setRequestHeader('X-WP-Nonce', configOptions.nonce);
                            },
                            contentType: 'application/json',
                            data: JSON.stringify({
                                "post_id": configOptions.postId,
                                "observation_name": configOptions.staticAttrSession,
                                "observation_value": configOptions.staticAttrSessionValStart
                            }),
                            success: function (data, textStatus, jQxhr) {
                                console.log("Success! " + JSON.stringify(data, null, 2));
                            }, // on success for add annotation
                            error: function (jqXhr, textStatus, errorThrown) {
                                console.log("Error " + errorThrown + ", " + textStatus + ", " + jqXhr.responseText);
                            }
                        }); // ajax call for session start


                    }, // get meta data success function
                    error: function (jqXhr, textStatus, errorThrown) {
                        console.log("Error " + errorThrown + ", " + textStatus + ", " + jqXhr.responseText);
                    }
                }); // ajax call to set up buttons from meta data
            }
        );

        jQuery('#' + configOptions.annotationStartButtonContainerId)
            .append(jqElemStartButton);


        let jqElemStopButton = jQuery(`       <button class="btn btn-primary btn-block stop-session ${configOptions.annotationButtonClassPost}"
                                                type="button"
                                                name="stop"
                                                value="stop"
                                                id="stop-session">
                                                END DATA COLLECTION
                                            </button>`);


        let sessionEndAjax = function (isAsynch, onSuccessCallback) {

            return (
                {
                    asynch: isAsynch,
                    url: configOptions.annotationUrl,
                    dataType: 'json',
                    type: 'post',
                    beforeSend: function (jqXhr) {
                        jqXhr.setRequestHeader('X-WP-Nonce', configOptions.nonce);
                    },
                    contentType: 'application/json',
                    data: JSON.stringify({
                        "post_id": configOptions.postId,
                        "observation_name": configOptions.staticAttrSession,
                        "observation_value": configOptions.staticAttrSessionValStop
                    }),
                    success: function (data, textStatus, jQxhr) {
                        onSuccessCallback(data);
                    }, // on success for add annotation
                    error: function (jqXhr, textStatus, errorThrown) {
                        console.log("Error " + errorThrown + ", " + textStatus + ", " + jqXhr.responseText);
                    }
                }
            );
        };

        // ajax will not work with onbeforeunload: you have to make it a blocking call
        // oddly, however, the fact that it blocks does not get in the way, for example,
        // of closing a tab. I tested with a 10 second sleep statement in the web service
        // and it did not get the way of either recording the stop event or the tab closing
        // immediately
        // window.onbeforeunload = jQuery.ajax(sessionEndAjax(false,
        //     function (data) {
        //         //; there is no lo longer a DOM or debug window to do anything with
        //     })); // ajax call for session stop

        jqElemStopButton.click(function () {
                // clear out all the containers we populated
                // jQuery('#' + configOptions.annotationVideoContainerId)
                //     .addClass('hidden')
                //     .empty();

                startAnnotation = false;

                jQuery("#" + configOptions.annotationButtonAddContainerId).empty();

                jQuery.ajax(sessionEndAjax(true,
                    function (data) {
                        jQuery('#' + configOptions.annotationStartButtonContainerId).show();
                        jQuery('#' + configOptions.annotationStopButtonContainerId).hide();

                        console.log("Success! " + JSON.stringify(data, null, 2));
                    })); // ajax call for session stop

                let jQueryEndButtonParagraph = jQuery(`<p class="new">${(new Date).toLocaleTimeString()}: Your current session has ended</p>`)
                jQuery("#" + configOptions.statusAreaContainerId)
                    .prepend(jQuery(jQueryEndButtonParagraph));
                
                // Timeout for "new" class on end status
                window.setTimeout(function () {
                    jQueryEndButtonParagraph.removeClass('new')
                }, 2000);

                // Interface session toggle - hide annotation, show thank you, then welcome, remove delete buttons from status
                jQuery('.bp-live_annotation-content[data-session="annotate"]').removeClass('show');
                jQuery('.bp-live_annotation-content[data-session="welcome"], .bp-live_annotation-content.welcome .thanks').addClass('show');
                window.setTimeout(function () {
                    jQuery('.bp-live_annotation-content.welcome .thanks').removeClass('show');
                }, 5000);
                jQuery('.bp-live_annotation-updates .scroll .annotation-button-delete').remove();

                jQuery('.status h4 span').show();

            }
        );


        jQuery('#' + configOptions.annotationStopButtonContainerId)
            .append(jqElemStopButton);


    }; // setUpChart


    // return determines the "public" methods of the module
    return {
        setUp: setUp
    }

    // Get time 12 hour time with am/pm
    function formatTime(date) {
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12;
        minutes = minutes < 10 ? '0' + minutes : minutes;
        var strTime = hours + ':' + minutes + ' ' + ampm;
        return strTime;
    }

})();


/*! nanoScrollerJS - v0.8.7 - (c) 2015 James Florentino; Licensed MIT */
!function (a) {
    return "function" == typeof define && define.amd ? define(["jquery"], function (b) {
        return a(b, window, document)
    }) : "object" == typeof exports ? module.exports = a(require("jquery"), window, document) : a(jQuery, window, document)
}(function (a, b, c) {
    "use strict";
    var d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z, A, B, C, D, E, F, G, H;
    z = {
        paneClass: "nano-pane",
        sliderClass: "nano-slider",
        contentClass: "nano-content",
        enabledClass: "has-scrollbar",
        flashedClass: "flashed",
        activeClass: "active",
        iOSNativeScrolling: !1,
        preventPageScrolling: !1,
        disableResize: !1,
        alwaysVisible: !1,
        flashDelay: 1500,
        sliderMinHeight: 20,
        sliderMaxHeight: null,
        documentContext: null,
        windowContext: null
    }, u = "scrollbar", t = "scroll", l = "mousedown", m = "mouseenter", n = "mousemove", p = "mousewheel", o = "mouseup", s = "resize", h = "drag", i = "enter", w = "up", r = "panedown", f = "DOMMouseScroll", g = "down", x = "wheel", j = "keydown", k = "keyup", v = "touchmove", d = "Microsoft Internet Explorer" === b.navigator.appName && /msie 7./i.test(b.navigator.appVersion) && b.ActiveXObject, e = null, D = b.requestAnimationFrame, y = b.cancelAnimationFrame, F = c.createElement("div").style, H = function () {
        var a, b, c, d, e, f;
        for (d = ["t", "webkitT", "MozT", "msT", "OT"], a = e = 0, f = d.length; f > e; a = ++e) if (c = d[a], b = d[a] + "ransform", b in F) return d[a].substr(0, d[a].length - 1);
        return !1
    }(), G = function (a) {
        return H === !1 ? !1 : "" === H ? a : H + a.charAt(0).toUpperCase() + a.substr(1)
    }, E = G("transform"), B = E !== !1, A = function () {
        var a, b, d;
        return a = c.createElement("div"), b = a.style, b.position = "absolute", b.width = "100px", b.height = "100px", b.overflow = t, b.top = "-9999px", c.body.appendChild(a), d = a.offsetWidth - a.clientWidth, c.body.removeChild(a), d
    }, C = function () {
        var a, c, d;
        return c = b.navigator.userAgent, (a = /(?=.+Mac OS X)(?=.+Firefox)/.test(c)) ? (d = /Firefox\/\d{2}\./.exec(c), d && (d = d[0].replace(/\D+/g, "")), a && +d > 23) : !1
    }, q = function () {
        function j(d, f) {
            this.el = d, this.options = f, e || (e = A()), this.$el = a(this.el), this.doc = a(this.options.documentContext || c), this.win = a(this.options.windowContext || b), this.body = this.doc.find("body"), this.$content = this.$el.children("." + this.options.contentClass), this.$content.attr("tabindex", this.options.tabIndex || 0), this.content = this.$content[0], this.previousPosition = 0, this.options.iOSNativeScrolling && null != this.el.style.WebkitOverflowScrolling ? this.nativeScrolling() : this.generate(), this.createEvents(), this.addEvents(), this.reset()
        }

        return j.prototype.preventScrolling = function (a, b) {
            if (this.isActive) if (a.type === f) (b === g && a.originalEvent.detail > 0 || b === w && a.originalEvent.detail < 0) && a.preventDefault(); else if (a.type === p) {
                if (!a.originalEvent || !a.originalEvent.wheelDelta) return;
                (b === g && a.originalEvent.wheelDelta < 0 || b === w && a.originalEvent.wheelDelta > 0) && a.preventDefault()
            }
        }, j.prototype.nativeScrolling = function () {
            this.$content.css({WebkitOverflowScrolling: "touch"}), this.iOSNativeScrolling = !0, this.isActive = !0
        }, j.prototype.updateScrollValues = function () {
            var a, b;
            a = this.content, this.maxScrollTop = a.scrollHeight - a.clientHeight, this.prevScrollTop = this.contentScrollTop || 0, this.contentScrollTop = a.scrollTop, b = this.contentScrollTop > this.previousPosition ? "down" : this.contentScrollTop < this.previousPosition ? "up" : "same", this.previousPosition = this.contentScrollTop, "same" !== b && this.$el.trigger("update", {
                position: this.contentScrollTop,
                maximum: this.maxScrollTop,
                direction: b
            }), this.iOSNativeScrolling || (this.maxSliderTop = this.paneHeight - this.sliderHeight, this.sliderTop = 0 === this.maxScrollTop ? 0 : this.contentScrollTop * this.maxSliderTop / this.maxScrollTop)
        }, j.prototype.setOnScrollStyles = function () {
            var a;
            B ? (a = {}, a[E] = "translate(0, " + this.sliderTop + "px)") : a = {top: this.sliderTop}, D ? (y && this.scrollRAF && y(this.scrollRAF), this.scrollRAF = D(function (b) {
                return function () {
                    return b.scrollRAF = null, b.slider.css(a)
                }
            }(this))) : this.slider.css(a)
        }, j.prototype.createEvents = function () {
            this.events = {
                down: function (a) {
                    return function (b) {
                        return a.isBeingDragged = !0, a.offsetY = b.pageY - a.slider.offset().top, a.slider.is(b.target) || (a.offsetY = 0), a.pane.addClass(a.options.activeClass), a.doc.bind(n, a.events[h]).bind(o, a.events[w]), a.body.bind(m, a.events[i]), !1
                    }
                }(this), drag: function (a) {
                    return function (b) {
                        return a.sliderY = b.pageY - a.$el.offset().top - a.paneTop - (a.offsetY || .5 * a.sliderHeight), a.scroll(), a.contentScrollTop >= a.maxScrollTop && a.prevScrollTop !== a.maxScrollTop ? a.$el.trigger("scrollend") : 0 === a.contentScrollTop && 0 !== a.prevScrollTop && a.$el.trigger("scrolltop"), !1
                    }
                }(this), up: function (a) {
                    return function (b) {
                        return a.isBeingDragged = !1, a.pane.removeClass(a.options.activeClass), a.doc.unbind(n, a.events[h]).unbind(o, a.events[w]), a.body.unbind(m, a.events[i]), !1
                    }
                }(this), resize: function (a) {
                    return function (b) {
                        a.reset()
                    }
                }(this), panedown: function (a) {
                    return function (b) {
                        return a.sliderY = (b.offsetY || b.originalEvent.layerY) - .5 * a.sliderHeight, a.scroll(), a.events.down(b), !1
                    }
                }(this), scroll: function (a) {
                    return function (b) {
                        a.updateScrollValues(), a.isBeingDragged || (a.iOSNativeScrolling || (a.sliderY = a.sliderTop, a.setOnScrollStyles()), null != b && (a.contentScrollTop >= a.maxScrollTop ? (a.options.preventPageScrolling && a.preventScrolling(b, g), a.prevScrollTop !== a.maxScrollTop && a.$el.trigger("scrollend")) : 0 === a.contentScrollTop && (a.options.preventPageScrolling && a.preventScrolling(b, w), 0 !== a.prevScrollTop && a.$el.trigger("scrolltop"))))
                    }
                }(this), wheel: function (a) {
                    return function (b) {
                        var c;
                        if (null != b) return c = b.delta || b.wheelDelta || b.originalEvent && b.originalEvent.wheelDelta || -b.detail || b.originalEvent && -b.originalEvent.detail, c && (a.sliderY += -c / 3), a.scroll(), !1
                    }
                }(this), enter: function (a) {
                    return function (b) {
                        var c;
                        if (a.isBeingDragged) return 1 !== (b.buttons || b.which) ? (c = a.events)[w].apply(c, arguments) : void 0
                    }
                }(this)
            }
        }, j.prototype.addEvents = function () {
            var a;
            this.removeEvents(), a = this.events, this.options.disableResize || this.win.bind(s, a[s]), this.iOSNativeScrolling || (this.slider.bind(l, a[g]), this.pane.bind(l, a[r]).bind("" + p + " " + f, a[x])), this.$content.bind("" + t + " " + p + " " + f + " " + v, a[t])
        }, j.prototype.removeEvents = function () {
            var a;
            a = this.events, this.win.unbind(s, a[s]), this.iOSNativeScrolling || (this.slider.unbind(), this.pane.unbind()), this.$content.unbind("" + t + " " + p + " " + f + " " + v, a[t])
        }, j.prototype.generate = function () {
            var a, c, d, f, g, h, i;
            return f = this.options, h = f.paneClass, i = f.sliderClass, a = f.contentClass, (g = this.$el.children("." + h)).length || g.children("." + i).length || this.$el.append('<div class="' + h + '"><div class="' + i + '" /></div>'), this.pane = this.$el.children("." + h), this.slider = this.pane.find("." + i), 0 === e && C() ? (d = b.getComputedStyle(this.content, null).getPropertyValue("padding-right").replace(/[^0-9.]+/g, ""), c = {
                right: -14,
                paddingRight: +d + 14
            }) : e && (c = {right: -e}, this.$el.addClass(f.enabledClass)), null != c && this.$content.css(c), this
        }, j.prototype.restore = function () {
            this.stopped = !1, this.iOSNativeScrolling || this.pane.show(), this.addEvents()
        }, j.prototype.reset = function () {
            var a, b, c, f, g, h, i, j, k, l, m, n;
            return this.iOSNativeScrolling ? void (this.contentHeight = this.content.scrollHeight) : (this.$el.find("." + this.options.paneClass).length || this.generate().stop(), this.stopped && this.restore(), a = this.content, f = a.style, g = f.overflowY, d && this.$content.css({height: this.$content.height()}), b = a.scrollHeight + e, l = parseInt(this.$el.css("max-height"), 10), l > 0 && (this.$el.height(""), this.$el.height(a.scrollHeight > l ? l : a.scrollHeight)), i = this.pane.outerHeight(!1), k = parseInt(this.pane.css("top"), 10), h = parseInt(this.pane.css("bottom"), 10), j = i + k + h, n = Math.round(j / b * i), n < this.options.sliderMinHeight ? n = this.options.sliderMinHeight : null != this.options.sliderMaxHeight && n > this.options.sliderMaxHeight && (n = this.options.sliderMaxHeight), g === t && f.overflowX !== t && (n += e), this.maxSliderTop = j - n, this.contentHeight = b, this.paneHeight = i, this.paneOuterHeight = j, this.sliderHeight = n, this.paneTop = k, this.slider.height(n), this.events.scroll(), this.pane.show(), this.isActive = !0, a.scrollHeight === a.clientHeight || this.pane.outerHeight(!0) >= a.scrollHeight && g !== t ? (this.pane.hide(), this.isActive = !1) : this.el.clientHeight === a.scrollHeight && g === t ? this.slider.hide() : this.slider.show(), this.pane.css({
                opacity: this.options.alwaysVisible ? 1 : "",
                visibility: this.options.alwaysVisible ? "visible" : ""
            }), c = this.$content.css("position"), ("static" === c || "relative" === c) && (m = parseInt(this.$content.css("right"), 10), m && this.$content.css({
                right: "",
                marginRight: m
            })), this)
        }, j.prototype.scroll = function () {
            return this.isActive ? (this.sliderY = Math.max(0, this.sliderY), this.sliderY = Math.min(this.maxSliderTop, this.sliderY), this.$content.scrollTop(this.maxScrollTop * this.sliderY / this.maxSliderTop), this.iOSNativeScrolling || (this.updateScrollValues(), this.setOnScrollStyles()), this) : void 0
        }, j.prototype.scrollBottom = function (a) {
            return this.isActive ? (this.$content.scrollTop(this.contentHeight - this.$content.height() - a).trigger(p), this.stop().restore(), this) : void 0
        }, j.prototype.scrollTop = function (a) {
            return this.isActive ? (this.$content.scrollTop(+a).trigger(p), this.stop().restore(), this) : void 0
        }, j.prototype.scrollTo = function (a) {
            return this.isActive ? (this.scrollTop(this.$el.find(a).get(0).offsetTop), this) : void 0
        }, j.prototype.stop = function () {
            return y && this.scrollRAF && (y(this.scrollRAF), this.scrollRAF = null), this.stopped = !0, this.removeEvents(), this.iOSNativeScrolling || this.pane.hide(), this
        }, j.prototype.destroy = function () {
            return this.stopped || this.stop(), !this.iOSNativeScrolling && this.pane.length && this.pane.remove(), d && this.$content.height(""), this.$content.removeAttr("tabindex"), this.$el.hasClass(this.options.enabledClass) && (this.$el.removeClass(this.options.enabledClass), this.$content.css({right: ""})), this
        }, j.prototype.flash = function () {
            return !this.iOSNativeScrolling && this.isActive ? (this.reset(), this.pane.addClass(this.options.flashedClass), setTimeout(function (a) {
                return function () {
                    a.pane.removeClass(a.options.flashedClass)
                }
            }(this), this.options.flashDelay), this) : void 0
        }, j
    }(), a.fn.nanoScroller = function (b) {
        return this.each(function () {
            var c, d;
            if ((d = this.nanoscroller) || (c = a.extend({}, z, b), this.nanoscroller = d = new q(this, c)), b && "object" == typeof b) {
                if (a.extend(d.options, b), null != b.scrollBottom) return d.scrollBottom(b.scrollBottom);
                if (null != b.scrollTop) return d.scrollTop(b.scrollTop);
                if (b.scrollTo) return d.scrollTo(b.scrollTo);
                if ("bottom" === b.scroll) return d.scrollBottom(0);
                if ("top" === b.scroll) return d.scrollTop(0);
                if (b.scroll && b.scroll instanceof a) return d.scrollTo(b.scroll);
                if (b.stop) return d.stop();
                if (b.destroy) return d.destroy();
                if (b.flash) return d.flash()
            }
            return d.reset()
        })
    }, a.fn.nanoScroller.Constructor = q
});