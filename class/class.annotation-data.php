<?php

// for ddl functionality, borrowed from https://codex.wordpress.org/Creating_Tables_with_Plugins#A_Version_Option

require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

define("COL_NAME_ID", "id", true);
define("COL_NAME_USER_ID", "contributor_user_id", true);
define("COL_NAME_UTC_TIME", "time_in_utc", true);
define("COL_NAME_STUDY_NAME", "study_name", true);
define("COL_NAME_FEED_ID", "feed_id", true);
define("COL_NAME_OBSERVATION_NAME", "observation_name", true);
define("COL_NAME_OBSERVATION_VALUE", "observation_value", true);
define("COL_NAME_USER_LOGIN", "user_login", true);
define("COL_NAME_USER_EMAIL", "user_email", true);
define("COL_NAME_DELETE_FLAG", "delete_flag", true);

define("STATIC_ANNOTATION_ATTR_VAL_SESSION", "session_status", true);
define("STATIC_ANNOTATION_ATTR_VAL_SESSION_START", "start", true);
define("STATIC_ANNOTATION_ATTR__VAL_SESSION_STOP", "stop", true);

class AnnotationData
{

    private static $file_fileds = array(
        "study_name",
        "feed_id",
        "utc_stamp",
        "user_id",
        "observation_id",
        "observation_value"
    );

    public static function get_study_name($post_id)
    {

        $post_meta_data = get_post_custom($post_id);
        $study_name = $post_meta_data[STUDY_NAME][0];

        return ($study_name);
    }

    public static function get_feed_id($post_id)
    {

        $post_meta_data = get_post_custom($post_id);
        $study_name = $post_meta_data[FEED_ID][0];

        return ($study_name);
    }


    public static function get_db_table_name()
    {
        global $wpdb;
        return ($wpdb->prefix . 'clo_feed_annotations');
    }

    // I have verified that:
    // * This code only fires on activation;
    // * When you deactivate the plugin, the table remains (which is what we want);
    // * When you reactivate the plugin, there are no errors caused by the fact that the table already exists
    //
    // According to https://codex.wordpress.org/Creating_Tables_with_Plugins, "Adding an Upgrade Function," it should
    // be possible to modify the create table DDL and just re-run it with dbDelta(). So, I _guess_ this works like
    // Liquibase? I just don't know what would happen if a DDL structure change would be destructive to existing
    // data?
    public static function db_initialize()
    {
        global $wpdb;

        $wpdb->show_errors();

        $installed_ver = get_option(PLUGIN_DB_VERSION_OPTION_NAME);
        $installed_version_option_missing = !isset($installed_ver) || empty($installed_ver);
        if ($installed_version_option_missing || ($installed_ver != PLUGIN_DB_VERSION)) {

            $table_name = self::get_db_table_name();

            $charset_collate = $wpdb->get_charset_collate();

            $sql = "CREATE TABLE $table_name (
                  " . COL_NAME_ID . " mediumint(9) NOT NULL AUTO_INCREMENT,
                  " . COL_NAME_USER_ID . " mediumint(9) NOT NULL,
                  " . COL_NAME_UTC_TIME . " datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
                  " . COL_NAME_STUDY_NAME . " tinytext NOT NULL,
                  " . COL_NAME_FEED_ID . " tinytext NOT NULL,
                  " . COL_NAME_OBSERVATION_NAME . " tinytext NOT NULL,
                  " . COL_NAME_OBSERVATION_VALUE . " tinytext NOT NULL,
                  " . COL_NAME_DELETE_FLAG . " TINYINT(1),
                  PRIMARY KEY  (" . COL_NAME_ID . ")
                ) $charset_collate;";

            dbDelta($sql);

            if ($wpdb->last_error !== '') {
                throw new Exception($wpdb->last_error);
            }

            // track db version
            if ($installed_version_option_missing == true) {
                add_option(PLUGIN_DB_VERSION_OPTION_NAME, PLUGIN_DB_VERSION);
            } else {
                update_option(PLUGIN_DB_VERSION_OPTION_NAME, PLUGIN_DB_VERSION);
            }

        } // if current db version does not match installed db version

    }

    public static function add_annotation($annotation_data)
    {

        $return_val = null;

        if (get_post_status($annotation_data->post_id)) {

            $study_name = self::get_study_name($annotation_data->post_id);
            $feed_id = self::get_feed_id($annotation_data->post_id);

            if (!empty($study_name) &&
                !empty($feed_id) &&
                !empty($annotation_data->observation_name) &&
                !empty($annotation_data->observation_value)) {

                $utc = current_time("mysql", 1);
                $user = wp_get_current_user();
                $user_id = $user->ID;


                $table_name = self::get_db_table_name();

                $insert_array = array(

                    COL_NAME_USER_ID => $user_id,
                    COL_NAME_UTC_TIME => $utc,
                    COL_NAME_STUDY_NAME => $study_name,
                    COL_NAME_FEED_ID => $feed_id,
                    COL_NAME_OBSERVATION_NAME => $annotation_data->observation_name,
                    COL_NAME_OBSERVATION_VALUE => $annotation_data->observation_value);


                global $wpdb;
                $wpdb->show_errors();

                $wpdb->insert($table_name,
                    $insert_array
                );

                if ($wpdb->last_error !== '') {
                    throw new Exception($wpdb->last_error);
                } else {
                    $return_val = $wpdb->insert_id;
                }
            } else {

                $message = "The post for post_id " . $annotation_data->post_id . " is missing required data: ";

                if (empty($study_name)) {
                    $message .= " study_name";
                }

                if (empty($feed_id)) {
                    $message .= " ,feed_id";
                }

                if(empty($annotation_data->observation_name)) {
                    $message .= ", observation_name";
                }
                if(empty($annotation_data->observation_value)) {
                    $message .= ", observation_value";
                }

                throw new Exception($message);

            }

        } else {
            throw new \mysql_xdevapi\Exception("Non-existent post-id: " . $annotation_data->post_id);
        }

        return $return_val;
    } // add_annotation()

    public static function get_annotation_by_id($annotation_id)
    {
        $return_val = null;

        global $wpdb;

        $table_name = self::get_db_table_name();

        $query = "SELECT * FROM " . $table_name . " WHERE " . COL_NAME_ID . " = " . $annotation_id;
        $results = $wpdb->get_results($query, ARRAY_A); // Query to fetch data from database table and storing in $results
        if (!empty($results)) {
            if ($wpdb->num_rows == 1) {

                $return_val = $results[0];

            } else {
                throw new Exception("Query for annotation_id " . $annotation_id . " returned 0 or more than one rows");
            }
        }

        return $return_val;
    }

    public static function delete_annotation_by_id($annotation_id)
    {
        $return_val = null;

        global $wpdb;

        $wpdb->show_errors();

        $table_name = self::get_db_table_name();

        $result = $wpdb->update(
            $table_name,
            array(
                COL_NAME_DELETE_FLAG => 1
            ),
            array(COL_NAME_ID => $annotation_id)
        );

        if ($result == true) {

            $return_val = self::get_annotation_by_id($annotation_id);

        } else {
            $error_message = " Error deleting annotation with id " . $annotation_id;
            if ($wpdb->last_error !== '') {
                $error_message .= ": " . $wpdb->last_error;
            }

            throw new Exception($error_message);

        }

        return $return_val;

    } // delete_annotation_by_id()


    public static function get_study_as_csv($study_name)
    {
        $return_val = null;

        global $wpdb;

        $table_name = self::get_db_table_name();

        $col_name_id = COL_NAME_ID;
        $col_name_user_login = COL_NAME_USER_LOGIN;
        $col_name_user_email = COL_NAME_USER_EMAIL;
        $col_name_time_in_utc = COL_NAME_UTC_TIME;
        $col_name_study_name = COL_NAME_STUDY_NAME;
        $col_name_observation_name = COL_NAME_OBSERVATION_NAME;
        $col_name_observation_value = COL_NAME_OBSERVATION_VALUE;
        $col_name_user_id = COL_NAME_USER_ID;
        $col_name_delete = COL_NAME_DELETE_FLAG;

        $query =
            <<<EOD
                SELECT a.$col_name_id,
                       u.$col_name_user_login, 
                       u.$col_name_user_email, 
                       a.$col_name_time_in_utc, 
                       a.$col_name_study_name, 
                       a.$col_name_observation_name, 
                       a.$col_name_observation_value, 
                       a.$col_name_delete 
                FROM   $table_name a 
                       JOIN wp_users u 
                         ON ( a.$col_name_user_id = u.id ) 
                       WHERE a.$col_name_study_name = '$study_name'
                       ORDER BY '$col_name_time_in_utc' DESC
EOD;


        $results = $wpdb->get_results($query, ARRAY_A); // Query to fetch data from database table and storing in $results
        if (!empty($results)) {
            $return_val = $col_name_id . "," .
                $col_name_user_login . "," .
                $col_name_user_email . "," .
                $col_name_time_in_utc . "," .
                $col_name_study_name . "," .
                $col_name_observation_name . "," .
                $col_name_observation_value . "," .
                $col_name_delete . "\n";
            foreach ($results as $current_row) {
                $return_val .= implode(",", $current_row) . "\n";
            }
        } else {
            $return_val = "There is no data for study " . $study_name;
        }

        return $return_val;
    }

}