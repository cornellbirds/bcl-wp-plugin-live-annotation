<?php

include_once("class.annotation-data.php");
include_once(plugin_dir_path(__FILE__) . "../class/class.live-annotation-meta-data.php");

class RestControllerLiveAnnotation extends WP_REST_Controller
{


    /**
     * Register the routes for the objects of the controller.
     * Most of this is boilerplate from the example in https://developer.wordpress.org/rest-api/extending-the-rest-api/adding-custom-endpoints/
     * I am commenting out all of the example but leaving it here for now just so that in the future we can see what the various options are
     */
    public function register_routes()
    {
        $version = '1';
        $namespace = 'clo-liveannotation/v' . $version;
        $base_annotations = 'annotations/';

        // this route will be available at http://birdcamslab03.dev.cc/wp-json/clo-liveannotation/v1/annotations
        register_rest_route($namespace, '/' . $base_annotations,
            array(
                'methods' => WP_REST_Server::CREATABLE,
                'callback' => array($this, 'post_annotation'),
                'permission_callback' => array($this, 'post_annotation_permissions_callback'),
                'args' => $this->get_endpoint_args_for_item_schema(true),
            )
        );

        register_rest_route($namespace, '/' . $base_annotations . '(?P<id>[\d]+)' . '/',
            array(
                'methods' => WP_REST_Server::DELETABLE,
                'callback' => array($this, 'delete_annotation'),
                'permission_callback' => array($this, 'delete_annotation_permissions_callback'),
                'args' => $this->get_endpoint_args_for_item_schema(true),
            )
        );

        register_rest_route($namespace, '/' . $base_annotations . 'metadata/(?P<id>\d+)',
            array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array($this, 'get_annotation_meta_data'),
                'permission_callback' => array($this, 'get_annotation_meta_data_permissions_callback'),
                'args' => $this->get_endpoint_args_for_item_schema(true),
            )
        );

        register_rest_route($namespace, '/' . $base_annotations . '/schema', array(
            'methods' => WP_REST_Server::READABLE,
            'callback' => array($this, 'get_public_item_schema'),
        ));


//        register_rest_route($namespace, '/' . $base, array(
//            array(
//                'methods' => WP_REST_Server::READABLE,
//                'callback' => array($this, 'get_items'),
//                'permission_callback' => array($this, 'get_items_permissions_check'),
//                'args' => array(),
//            ),
//            array(
//                'methods' => WP_REST_Server::CREATABLE,
//                'callback' => array($this, 'create_item'),
//                'permission_callback' => array($this, 'create_item_permissions_check'),
//                'args' => $this->get_endpoint_args_for_item_schema(true),
//            ),
//        ));
//        register_rest_route($namespace, '/' . $base . '/(?P<id>[\d]+)', array(
//            array(
//                'methods' => WP_REST_Server::READABLE,
//                'callback' => array($this, 'get_item'),
//                'permission_callback' => array($this, 'get_item_permissions_check'),
//                'args' => array(
//                    'context' => array(
//                        'default' => 'view',
//                    ),
//                ),
//            ),
//            array(
//                'methods' => WP_REST_Server::EDITABLE,
//                'callback' => array($this, 'update_item'),
//                'permission_callback' => array($this, 'update_item_permissions_check'),
//                'args' => $this->get_endpoint_args_for_item_schema(false),
//            ),
//            array(
//                'methods' => WP_REST_Server::DELETABLE,
//                'callback' => array($this, 'delete_item'),
//                'permission_callback' => array($this, 'delete_item_permissions_check'),
//                'args' => array(
//                    'force' => array(
//                        'default' => false,
//                    ),
//                ),
//            ),
//        ));
//        register_rest_route($namespace, '/' . $base . '/schema', array(
//            'methods' => WP_REST_Server::READABLE,
//            'callback' => array($this, 'get_public_item_schema'),
//        ));
    } // register_routes()


    /// for info on getting parameters,
    ///  see https://developer.wordpress.org/rest-api/extending-the-rest-api/adding-custom-endpoints/#arguments
    ///
    ///
    ///

    public function post_annotation_permissions_callback($request)
    {
        return is_user_logged_in();
    }

    public function delete_annotation_permissions_callback($request)
    {
        return is_user_logged_in();
    }

    public function get_annotation_meta_data_permissions_callback($request)
    {
        return is_user_logged_in();
    }
    // The expected JSON body has this structure:
    //  {
    //      "post_id":1862,
    //      "observation_name":"fruit_added",
    //      "observation_value":"pear"
    //  }
    public function post_annotation($request)
    {
        $return_val = null;

        $request_body = $request->get_body();
        $json_object = json_decode($request_body);

        if (isset($json_object)) {

            try {

                $annotation_id = AnnotationData::add_annotation($json_object);

                if (!empty($annotation_id)) {

                    $committed_annotation = AnnotationData::get_annotation_by_id($annotation_id);
                    $return_val = new WP_REST_Response($committed_annotation, 200);

                } else {
                    $return_val = new WP_Error(500, __("Error processing annotation: " . $request_body . ": the annotation was not stored", 'text-domain'));
                }

            } catch (Exception $e) {
                $return_val = new WP_Error(500, __("Error processing annotation: " . $request_body . ": " . $e->getMessage(), 'text-domain'));
            }
        } else {
            $return_val = new WP_Error(500, __("Unable to parse request body to JSON: " . $request_body, 'text-domain'));
        }


        return $return_val;

    } // post_annotation()

    public function delete_annotation($request)
    {
        $return_val = null;

        $annotation_id = $annotation_id = $request->get_param('id');
        try {
            
            if (!empty($annotation_id)) {

                AnnotationData::delete_annotation_by_id($annotation_id);
                $committed_annotation = AnnotationData::get_annotation_by_id($annotation_id);
                $return_val = new WP_REST_Response($committed_annotation, 200);

            } else {
                $return_val = new WP_Error(500, __("Error processing annotation: the annotation id is empty", 'text-domain'));
            }

        } catch (Exception $e) {
            $return_val = new WP_Error(500, __("Error processing annotation " . $annotation_id . ": " . $e->getMessage(), 'text-domain'));
        }


        return $return_val;

    } // post_annotation()

    public function get_annotation_meta_data($request)
    {

        $return_val = null;
        $post_id = $request->get_param('id');

        try {

            $live_annotation_meta_data = new LiveAnnotationMetaData();
            $message = null;
            $annotation_meta_data = $live_annotation_meta_data->get_annotation_meta_data($post_id, $message);
            if ($message == null) {
                $return_val = new WP_REST_Response($annotation_meta_data, 200);
            } else {
                $return_val = new WP_Error(500, __("Error processing annotation: " . $request_body . ": " . $e->getMessage(), 'text-domain'));
            }

        } catch (Exception $e) {

            $return_val = new WP_Error(500, __("Error processing annotation: " . $request_body . ": " . $e->getMessage(), 'text-domain'));
        }


        return $return_val;

    } // get_annotation_meta_data()

//
//    /**
//     * Get a collection of items
//     *
//     * @param WP_REST_Request $request Full data about the request.
//     * @return WP_Error|WP_REST_Response
//     */
//    public function get_items($request)
//    {
//        $items = array(); //do a query, call another class, etc
//        $data = array();
//        foreach ($items as $item) {
//            $itemdata = $this->prepare_item_for_response($item, $request);
//            $data[] = $this->prepare_response_for_collection($itemdata);
//        }
//
//        return new WP_REST_Response($data, 200);
//    }
//
//    /**
//     * Get one item from the collection
//     *
//     * @param WP_REST_Request $request Full data about the request.
//     * @return WP_Error|WP_REST_Response
//     */
//    public function get_item($request)
//    {
//        //get parameters from request
//        $params = $request->get_params();
//        $item = array();//do a query, call another class, etc
//        $data = $this->prepare_item_for_response($item, $request);
//
//        //return a response or error based on some conditional
//        if (1 == 1) {
//            return new WP_REST_Response($data, 200);
//        } else {
//            return new WP_Error('code', __('message', 'text-domain'));
//        }
//    }
//
//    /**
//     * Create one item from the collection
//     *
//     * @param WP_REST_Request $request Full data about the request.
//     * @return WP_Error|WP_REST_Request
//     */
//    public function create_item($request)
//    {
//        $item = $this->prepare_item_for_database($request);
//
//        if (function_exists('slug_some_function_to_create_item')) {
//            $data = slug_some_function_to_create_item($item);
//            if (is_array($data)) {
//                return new WP_REST_Response($data, 200);
//            }
//        }
//
//        return new WP_Error('cant-create', __('message', 'text-domain'), array('status' => 500));
//    }
//
//    /**
//     * Update one item from the collection
//     *
//     * @param WP_REST_Request $request Full data about the request.
//     * @return WP_Error|WP_REST_Request
//     */
//    public function update_item($request)
//    {
//        $item = $this->prepare_item_for_database($request);
//
//        if (function_exists('slug_some_function_to_update_item')) {
//            $data = slug_some_function_to_update_item($item);
//            if (is_array($data)) {
//                return new WP_REST_Response($data, 200);
//            }
//        }
//
//        return new WP_Error('cant-update', __('message', 'text-domain'), array('status' => 500));
//    }
//
//    /**
//     * Delete one item from the collection
//     *
//     * @param WP_REST_Request $request Full data about the request.
//     * @return WP_Error|WP_REST_Request
//     */
//    public function delete_item($request)
//    {
//        $item = $this->prepare_item_for_database($request);
//
//        if (function_exists('slug_some_function_to_delete_item')) {
//            $deleted = slug_some_function_to_delete_item($item);
//            if ($deleted) {
//                return new WP_REST_Response(true, 200);
//            }
//        }
//
//        return new WP_Error('cant-delete', __('message', 'text-domain'), array('status' => 500));
//    }
//
//    /**
//     * Check if a given request has access to get items
//     *
//     * @param WP_REST_Request $request Full data about the request.
//     * @return WP_Error|bool
//     */
//    public function get_items_permissions_check($request)
//    {
//        //return true; <--use to make readable by all
//        return current_user_can('edit_something');
//    }
//
//    /**
//     * Check if a given request has access to get a specific item
//     *
//     * @param WP_REST_Request $request Full data about the request.
//     * @return WP_Error|bool
//     */
//    public function get_item_permissions_check($request)
//    {
//        return $this->get_items_permissions_check($request);
//    }
//
//    /**
//     * Check if a given request has access to create items
//     *
//     * @param WP_REST_Request $request Full data about the request.
//     * @return WP_Error|bool
//     */
//
//    /**
//     * Check if a given request has access to update a specific item
//     *
//     * @param WP_REST_Request $request Full data about the request.
//     * @return WP_Error|bool
//     */
//    public function update_item_permissions_check($request)
//    {
//        return $this->create_item_permissions_check($request);
//    }
//
//    /**
//     * Check if a given request has access to delete a specific item
//     *
//     * @param WP_REST_Request $request Full data about the request.
//     * @return WP_Error|bool
//     */
//    public function delete_item_permissions_check($request)
//    {
//        return $this->create_item_permissions_check($request);
//    }
//
//    /**
//     * Prepare the item for create or update operation
//     *
//     * @param WP_REST_Request $request Request object
//     * @return WP_Error|object $prepared_item
//     */
//    protected function prepare_item_for_database($request)
//    {
//        return array();
//    }
//
//    /**
//     * Prepare the item for the REST response
//     *
//     * @param mixed $item WordPress representation of the item.
//     * @param WP_REST_Request $request Request object.
//     * @return mixed
//     */
//    public function prepare_item_for_response($item, $request)
//    {
//        return array();
//    }
//
//    /**
//     * Get the query params for collections
//     *
//     * @return array
//     */
//    public function get_collection_params()
//    {
//        return array(
//            'page' => array(
//                'description' => 'Current page of the collection.',
//                'type' => 'integer',
//                'default' => 1,
//                'sanitize_callback' => 'absint',
//            ),
//            'per_page' => array(
//                'description' => 'Maximum number of items to be returned in result set.',
//                'type' => 'integer',
//                'default' => 10,
//                'sanitize_callback' => 'absint',
//            ),
//            'search' => array(
//                'description' => 'Limit results to those matching a string.',
//                'type' => 'string',
//                'sanitize_callback' => 'sanitize_text_field',
//            ),
//        );
//    }
}

?>