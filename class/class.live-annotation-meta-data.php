<?php

define("JSON_KEY_BUTTONS", "buttons", true);
define("JSON_KEY_DATA_ATTRIBUTE_NAME", "data_attribute_name", true);
define("JSON_KEY_DATA_ATTRIBUTE_VALUE", "data_attribute_value", true);
define("JSON_KEY_LABEL", "label", true);
define("JSON_KEY_IMAGE_FILE_NAME", "image_file_name", true);


class LiveAnnotationMetaData
{


    public function get_feed_id($post_id)
    {
        $meta_data = $this->get_meta_data($post_id);
        return ($meta_data[FEED_ID][0]);
    }

    public function get_study_name($post_id)
    {
        $meta_data = $this->get_meta_data($post_id);
        return ($meta_data[STUDY_NAME][0]);
    }

    public function get_button_definitions($post_id)
    {
        $meta_data = $this->get_meta_data($post_id);
        return ($meta_data[BUTTON_DEFINITIONS][0]);
    }

    /// Returns an associative array containing the meta data for the post
    public function get_meta_data($post_id)
    {
        // we may change how we do this later, so for now encapsulate
        return (get_post_custom($post_id));

    }

    private function get_post_id_for_page_type($post_id, $page_type, &$message)
    {

        $return_val = 0;
        $meta_data = $this->get_meta_data($post_id);
        $page_permalink = $meta_data[$page_type][0];

        if (isset($page_permalink) && !empty($page_permalink)) {

            $return_val = url_to_postid($page_permalink);

            if (!isset($return_val) || empty($return_val)) {
                $message = "There is no post for permalink " . $page_permalink . " for page type " . $page_type;
            }

        } else {

            $message = "There is no " . $page_type . " configuration value set for this post";

        }

        return ($return_val);

    } // get_post_id_for_page_type()

    public function get_tutorial_page_post_id($post_id, &$message)
    {

        return ($this->get_post_id_for_page_type($post_id, TUTORIAL_PAGE,$message));

    }

    public function get_field_guide_page_post_id($post_id, &$message)
    {
        return ($this->get_post_id_for_page_type($post_id, FIELD_GUIDE_PAGE, $message));

    }

    // Returns an associative array of buttons from the post's JSON;
    // If the JSON doesn't validate, the return value will be null
    // and $message will contain the error message
    public function get_annotation_meta_data($post_id, &$message)
    {

        $returnVal = null;

        $button_json = $this->get_button_definitions($post_id);
        if (!empty($button_json)) {

            $button_definitions_json_obj = null;

            $button_definitions_json_obj = json_decode($button_json, true);
            if ($button_definitions_json_obj == null) {
                $message = "Unable to parse button definitions: "
                    . json_last_error_msg()
                    . ": "
                    . $button_json;

            } else {

                $buttons = $button_definitions_json_obj[JSON_KEY_BUTTONS];
                if (isset($buttons)) {

                    $buttons_ok = true;
                    for ($idx = 0; $buttons_ok && ($idx < sizeof($buttons)); $idx++) {

                        $current_button = $buttons[$idx];
                        if (!empty($current_button[JSON_KEY_DATA_ATTRIBUTE_NAME])) {

                            if (!empty($current_button[JSON_KEY_DATA_ATTRIBUTE_VALUE])) {

                                if (empty($current_button[JSON_KEY_LABEL])) {
                                    $buttons_ok = false;
                                    $message = "The button at array offset " . $idx . " has no value for the " . JSON_KEY_LABEL . " key";
                                } else {

                                    $image_name = $current_button[JSON_KEY_IMAGE_FILE_NAME];
                                    if (!empty($image_name)) {

                                        $path_to_image = $this->get_path_to_jpg_image($post_id, $image_name);
                                        if (!isset($path_to_image) || empty($path_to_image)) {
                                            $buttons_ok = false;
                                            $message = "There is no uploaded image for the image specified at array offset " . $idx . ": " . $image_name;
                                        } else {
                                            // the $current_button shorthand works for reading a value from a nested array;
                                            // but to write a value to the array, you have to specify the complete path to it :-(
                                            $buttons[$idx][JSON_KEY_IMAGE_FILE_NAME] = $path_to_image;
                                        }
                                    }

                                }

                            } else {
                                $buttons_ok = false;
                                $message = "The button at array offset " . $idx . " has no value for the " . JSON_KEY_DATA_ATTRIBUTE_VALUE . " key";
                            }

                        } else {
                            $buttons_ok = false;
                            $message = "The button at array offset " . $idx . " has no value for the " . JSON_KEY_DATA_ATTRIBUTE_NAME . " key";
                        }

                    } // iterate button definitions

                    if ($buttons_ok) {
                        $returnVal = $buttons;
                    } else {
                        $message = "Error parsing JSON: " . $message . ": " . $button_json;
                    }

                } else {

                    $message = "The JSON does not contain a value for the " . JSON_KEY_BUTTONS . " key";

                } // if-else there's a buttons array

            } // if-else JSON decode succeeded

        } else {
            $message = "No button definitions were provided";
        } // if else the json parameter has a value


        return $returnVal;
    }


    public function get_path_to_jpg_image($parent_post_id, $image_name)
    {

        $return_val = null;

        $attachments = get_children(array(
            'post_parent' => $parent_post_id,
            'post_type' => 'attachment',

        ));

        foreach ($attachments as $current_attachment) {
            if (strpos($current_attachment->guid, $image_name) != false) {
                $return_val = $current_attachment->guid;
                break;
            }
        }

//        if (count($attachments) == 1) {
//            $return_val = $attachments[array_keys($attachments)[0]];
//        }

        return $return_val;
    }


}

?>