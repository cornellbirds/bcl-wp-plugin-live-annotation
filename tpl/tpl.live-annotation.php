<?php
/*
Template Name: live_annotation
Template Post Type: live_annotation
 */

include_once(plugin_dir_path(__FILE__) . "../class/class.live-annotation-meta-data.php");
include_once(WP_PLUGIN_DIR . '/clo-wp-plugin-common/class/class.file-download.php');

// *********************************************************
// ************ BUILD THE DATA WE WILL NEED FOR THE TEMPLATE
$post_errors = "";

// ----------------- set up data
$post_id = get_the_ID();
$live_annotation_meta_data = new LiveAnnotationMetaData();
$feed_id = $live_annotation_meta_data->get_feed_id($post_id);
$study_name = $live_annotation_meta_data->get_study_name($post_id);
$button_definitions = $live_annotation_meta_data->get_button_definitions($post_id);
$feed_url = "https://www.youtube.com/embed/" . $feed_id;
$annotation_button_class_post = "annotation-button-post";
$annotation_button_class_delete = "annotation-button-delete";
$annotation_video_container_id = "bp-live-video-container";
$annotation_add_button_container_id = "bp-live-annotation-add-container";
$annotation_status_area_container_id = "bp-live-annotation-delete-container";
$annotation_start_button_container_id = "bp-live-start-button-container";
$annotation_stop_button_container_id = "bp-live-stop-button-container";
$nonce = wp_create_nonce('wp_rest');

// Get guide & tutorial content from pages
$tutorial_id = $live_annotation_meta_data->get_tutorial_page_post_id($post_id,$post_errors);
$tutorial_post = get_post($tutorial_id);
$tutorial_content = apply_filters('the_content', $tutorial_post->post_content);
$tutorial_title = $tutorial_post->post_title;

$fieldguide_id = $live_annotation_meta_data->get_field_guide_page_post_id($post_id, $post_errors);
$fieldguide_post = get_post($fieldguide_id);
$fieldguide_content = apply_filters('the_content', $fieldguide_post->post_content);
$fieldguide_title = $fieldguide_post->post_title;


// --------------- validation

$buttons = null;
if (empty($feed_id)) {
    $post_errors = "Feed ID is required";
} else {
    $buttons = $live_annotation_meta_data->get_annotation_meta_data($post_id, $post_errors);
}


?>


<!--*********************************************************-->
<!--************ NOW CONSUME THE DATA WE BUILT TO CREATE THE PAGE FROM THE TEMPLATE-->

<?php get_header(); ?>

<?php if (empty($post_errors)): ?>

    <div class="wrap content col-1 clearfix">
        <?php get_template_part('subnav'); ?>
        <article class="grid" id="bp-live_annotation-container" role="article">

            <h1><?php the_title(); ?></h1>
            <div id="annotation-container">
                <div class="row grid-layout">

                    <!-- BEGIN VIDEO this div holds the live feed -->
                    <div class="bp-live_annotation-feed">

                        <div class="bp-live_annotation-video-container">
                            <span class="feed-label">Live Feed</span>
                            <div class="embed-responsive embed-responsive-16by9">
                                <div id="bp-live-video-container" class="hidden"></div>

                                <script>
                                </script>
                                <div class="intro-text" id="bp-intro-text">
                                    <h2>Welcome to Hawk Happenings</h2>
                                    <p>Please read the <a class="learn-button" href="#annotation-tutorial">tutorial here</a>. 
                                    Click START when you are ready to start the live stream.</p>
                                </div>
                                <div class="cover-video" id="bp-cover-video"></div>
                            </div><!-- .embed-responsive-->
                            <a class="learn-back" href="#bp-live_annotation-container">Return to top</a>
                        </div><!-- .bp-live_annotation-video-container-->

                    </div><!-- .bp-live_annotation-feed  -->
                    <!-- END VIDEO -->

                    <!-- BEGIN SIDEBAR: this div holds all buttons and interactions -->
                    <div class="bp-live_annotation-aside">

                        <!-- BEGIN welcome content -->
                        <div class="bp-live_annotation-content welcome show" data-session="welcome">

                            <!-- BEGIN thank you note -->
                            <div class="thanks">
                                <h3>Thank you for your observations</h3>
                            </div>
                            <!-- END thank you note -->

                            <div class="bp-live_annotation-description">
                                <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" target="_blank">upgrade your browser</a> to improve your experience.</p>
                                <?php
                                the_post();
                                echo the_content();
                                ?>
                                <!-- <p><a class="species-toggle" rel="noopener noreferrer" data-toggle="modal" data-target="#bp-live_annotation-species">Learn which species you’ll ID</a></p> -->
                                <a class="btn btn-secondary btn-block learn-button" href="#annotation-tutorial">TUTORIAL</a>
                            </div> <!-- live_annotation-description -->

                            <?php //** SHOW IF LOGGED IN **\\
                            if (is_user_logged_in()): ?>

                                <!-- <button class="btn btn-secondary btn-block tutorial-toggle" data-toggle="modal" data-target="#bp-live_annotation-tutorial">DATA-ENTRY TUTORIAL</button> -->
                                

                                <div id="<?= $annotation_start_button_container_id ?>"></div>

                            <?php else: //** SHOW IF NOT LOGGED IN **\\ ?>
                                <p><em>You must be logged in to participate</em></p>
                                <a class="btn btn-primary btn-block" href="<?php echo wp_login_url(get_permalink()) ?>">LOG
                                    IN</a>
                            <?php endif; ?>

                        </div><!-- .bp-live_annotation-content-->
                        <!-- END welcome content -->

                        <!-- BEGIN annotation -->
                        <div class="bp-live_annotation-content annotate" data-session="annotate">
                            <div class="interact">
                                <div class="head">
                                    <h3>Click behaviors as they happen</h3>
                                </div><!-- .head -->

                                <!-- Annotation Buttons -->
                                <div id="<?= $annotation_add_button_container_id ?>"
                                  class="bp-live_annotation-buttons nest-cam">

                                </div>

                            </div><!-- .interact -->

                            <div id="<?= $annotation_stop_button_container_id ?>"></div>
                        </div>
                        <!-- END annotation  -->

                    </div><!-- .bp-live_annotation-aside  -->
                    <!-- END SIDEBAR -->

                    <!-- BEGIN STATUS: this div holds all status feedback and widgets under the feed -->
                    <div class="bp-live_annotation-info">

                        <div class="bp-live_annotation-bottom row">
                            <!-- BEGIN weather widget -->
                            <div class="info-side col-sm-4">
                                <div class="weather-widget show" data-session="welcome">
                                    <div id="weather-widget">
                                        <a class="weatherwidget-io" href="https://forecast7.com/en/42d44n76d50/ithaca/?unit=us" data-label_1="Ithaca, New York" data-textcolor="#ffffff" data-font="Open Sans" data-mode="Current" >Ithaca, New York</a>
                                        <script>
                                        !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src='https://weatherwidget.io/js/widget.min.js';fjs.parentNode.insertBefore(js,fjs);}}(document,'script','weatherwidget-io-js');
                                        </script>
                                    </div><!-- #weather-widget -->
                                </div>
                                <!-- END weather-widget -->

                                <!-- BEGIN tutorial links -->
                                <div class="links text-center" data-session="annotate">
                                    <a class="btn btn-full learn-button" href="#annotation-field-guide">FIELD GUIDE</a>
                                    <a class="btn btn-full learn-button" href="#annotation-tutorial">TUTORIAL</a>
                                </div>
                                <!-- END weather-widget -->
                            </div><!-- .bp-live_annotation-side -->

                            <!-- BEGIN status -->
                            <div class="info col-sm-8">
                                <div class="bp-live_annotation-main welcome show" data-session="welcome">
                                    <p><a href="https://birdcamslab.allaboutbirds.org/welcome-to-the-new-hawk-investigation/" target="_blank"><span>Learn More About The Project</span></a></p>
                                </div>

                                <div class="bp-live_annotation-main status" data-session="annotate">
                                    <h4><span class="thanks">Thanks! </span>This session you reported</h4>
                                    <div class="bp-live_annotation-updates nano">
                                        <div id="<?= $annotation_status_area_container_id ?>"
                                             class="scroll nano-content"></div>
                                    </div>
                                </div>
                            </div><!-- .info -->
                            <!-- END status -->

                        </div><!-- .bp-live_annotation-bottom  -->
                    </div><!-- .bp-live_annotation-info  -->
                    <!-- END STATUS -->

                </div><!-- .row -->

            </div><!-- .annotation-container  -->


            <div id="annotation-field-guide" class="learn-container guide">
                <h3><?php echo $fieldguide_title; ?></h3>
                <?php echo $fieldguide_content; ?>
            </div><!-- fieldguide -->

            <div id="annotation-tutorial" class="learn-container tutorial">
                <h3><?php echo $tutorial_title; ?></h3>
                <?php echo $tutorial_content; ?>
            </div><!-- fieldguide -->

            <?php if (current_user_can('edit_post')): ?>
                <p class="download-csv"><?= FileDownload::make_download_link($post_id, $study_name, DOWNLOAD_TYPE_DIRECT, LIVE_ANNOTATION_POST_TYPE_NAME, "Download CSV") ?></p>
            <?php endif; ?>

        </article><!-- #bp-live_annotation-container -->

    </div><!-- .wrap -->

    <script>
        window.onload = function () {

            CLO_LIVE_ANNOTATION.setUp(
                {
                    nonce: '<?= $nonce ?>',
                    annotationUrl: '/wp-json/clo-liveannotation/v1/annotations/',
                    metaDataUrl: '/wp-json/clo-liveannotation/v1/annotations/metadata/',
                    postId: <?= $post_id ?>,
                    feedUrl: '<?= $feed_url ?>',
                    annotationVideoContainerId: '<?=$annotation_video_container_id?>',
                    annotationStartButtonContainerId: '<?=$annotation_start_button_container_id?>',
                    annotationStopButtonContainerId: '<?=$annotation_stop_button_container_id?>',
                    annotationButtonAddContainerId: '<?=$annotation_add_button_container_id?>',
                    statusAreaContainerId: '<?=$annotation_status_area_container_id?>',
                    annotationButtonClassPost: '<?= $annotation_button_class_post ?>',
                    annotationButtonClassDelete: '<?= $annotation_button_class_delete ?>',
                    staticAttrSession: '<?=STATIC_ANNOTATION_ATTR_VAL_SESSION?>',
                    staticAttrSessionValStart: '<?=STATIC_ANNOTATION_ATTR_VAL_SESSION_START?>',
                    staticAttrSessionValStop: '<?=STATIC_ANNOTATION_ATTR__VAL_SESSION_STOP?>'
                });
        };
    </script>

<?php else: ?>

    <pre><?= $post_errors ?></pre>

<?php endif; ?>

<?php //** SHOW IF LOGGED IN **\\
if (is_user_logged_in()): ?>

    <?php get_template_part('next-prev'); ?>

    <?php //get_template_part('comment-area'); ?>
    <?php if (comments_open()) { ?>
        <section id="comment-area" class="container">
            <script>
                var disqus_config = function () {
                    this.page.url = '<?php get_the_permalink() ?>';
                    this.page.identifier = '<?php get_the_ID() ?>';
                };
                (function () {
                    var d = document, s = d.createElement('script');
                    s.src = 'https://birdcams-lab.disqus.com/embed.js';
                    s.setAttribute('data-timestamp', +new Date());
                    (d.head || d.body).appendChild(s);
                })();
            </script>
            <div id="disqus_thread"></div>
        </section>
    <?php } ?>

<?php endif; // End logged in ?>

<?php get_footer(); ?>