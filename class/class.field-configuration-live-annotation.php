<?php

include_once(WP_PLUGIN_DIR . '/clo-wp-plugin-common/class/field_editor/interface.field_configuration.php');
include_once(WP_PLUGIN_DIR . '/clo-wp-plugin-common/class/field_editor/class.field_utils.php');


class FieldConfigurationLiveAnnotation implements iFieldConfiguration
{
    public function get_field_definitions()
    {
        return (

        array(
            array(
                FLD_DEF_KEY_NAME => STUDY_NAME,
                FLD_DEF_KEY_TITLE => 'Study Name',
                FLD_DEF_KEY_DESC => 'Name of the study',
                FLD_DEF_KEY_TYPE => FIELD_TYPE_TEXT,
            ),
            array(
                FLD_DEF_KEY_NAME => FEED_ID,
                FLD_DEF_KEY_TITLE => 'Feed ID',
                FLD_DEF_KEY_DESC => 'Specify the ID of the YouTube Feed',
                FLD_DEF_KEY_TYPE => FIELD_TYPE_TEXT,
            ),
            array(
                FLD_DEF_KEY_NAME => BUTTON_DEFINITIONS,
                FLD_DEF_KEY_TITLE => 'Button Definitions',
                FLD_DEF_KEY_DESC => 'Provide the JSON document to define the buttons and their attributes that will be used to record annotation data',
                FLD_DEF_KEY_TYPE => FIELD_TYPE_TEXTAREA,
            ),
            array(
                FLD_DEF_KEY_NAME => TUTORIAL_PAGE,
                FLD_DEF_KEY_TITLE => 'Tutorial Page',
                FLD_DEF_KEY_DESC => 'Permalink for tutorial page',
                FLD_DEF_KEY_TYPE => FIELD_TYPE_TEXT,
            ),
            array(
                FLD_DEF_KEY_NAME => FIELD_GUIDE_PAGE,
                FLD_DEF_KEY_TITLE => 'Field Guide Page',
                FLD_DEF_KEY_DESC => 'Permalink for field guide page',
                FLD_DEF_KEY_TYPE => FIELD_TYPE_TEXT,
            ),
        )
        );
    } // getFieldDefinitions()

    public function get_fields_to_remove()
    {
        // to remove media uploader: "ij-post-attachments",
        return (array("postcustom", "customsidebars-mb"));
    }

    public function get_post_type()
    {
        return (LIVE_ANNOTATION_POST_TYPE_NAME);
    }

    public function get_meta_box_id()
    {
        return (META_BOX_ID);
    }

    public function get_meta_box_title()
    {
        return (META_BOX_TITLE);
    }

    public function get_field_prefix()
    {
        return ('ANT_FIELD_');
    }


    public function get_admin_page_script()
    {
        return (""); //no script for now

    }

    public function get_is_hide_editor()
    {
        return false;
    }


} // FieldConfigurationVisualization