<?php

/*
 * Plugin Name: CLO Live Annotation
 * Plugin URI:
 * Description: Live Annotation of CLO Video Feeds
 * Version: 1.0.0
 * Author: Phil Glaser
 * Author URI:
 * License:
 */


// we don't need the template builder unless we need to make the template selectable from an ordinary page post.
// What we're doing now, instead, is associating the template directly with the live_annotation post type
// include_once("class/class.live_annotation_template_builder.php"). In other words, the only context in which
// the

//include_once("class/class.rest_controller.php");
//include_once("class/class.file-download.php");
include_once("class/class.field-configuration-live-annotation.php");
include_once("class/class.rest-controller-live-annotation.php");
include_once("class/class.annotation-data.php");
include_once(WP_PLUGIN_DIR . '/clo-wp-plugin-common/class/class.post_type_config.php');
//include_once(WP_PLUGIN_DIR . '/clo-wp-plugin-common/class/field_editor/class.field_editor.php');


define("LIVE_ANNOTATION_POST_TYPE_NAME", "liveannotation", true);
define("LIVE_ANNOTATION_POST_TYPE_LABEL", "Live Annotation", true);


define("META_BOX_ID", "LIVE-ANNOTATION-FIELDS", true);
define("META_BOX_TITLE", "Configure Live Annotation");
define("FEED_ID", "FEED-ID", true);
define("BUTTON_DEFINITIONS", "BUTTON-DEFINITIONS", true);
define("STUDY_NAME", "STUDY-NAME", true);
define("TUTORIAL_PAGE", "TUTORIAL-PAGE", true);
define("FIELD_GUIDE_PAGE", "FIELD-GUIDE-PAGE", true);
define("PLUGIN_DB_VERSION_OPTION_NAME", "live-annotation-db-version", true);
define("PLUGIN_DB_VERSION", "1.1", true);

function register_live_annotation_post_type()
{
    if (class_exists('PostTypeConfig')) {

        $post_config_array = PostTypeConfig::make_post_type_config(LIVE_ANNOTATION_POST_TYPE_LABEL,
            array(
                'template',
                'title',
                'editor', // we need editor for upload button but will hide editor box
                'custom-fields',
                'page-attributes',
                'comments',
                'discussion'));

        register_post_type(LIVE_ANNOTATION_POST_TYPE_NAME, $post_config_array);
    }
}


function associate_live_annotation_template($template)
{
    $post_types = array(LIVE_ANNOTATION_POST_TYPE_NAME);
    if (is_singular($post_types)) {
        $template = plugin_dir_path(__FILE__) . 'tpl/tpl.live-annotation.php';
    }

    return $template;
}

function initialize_plugin_live_annotation()
{
// initialization hooks
    add_action('init', 'register_live_annotation_post_type');


//add_action('init', 'live_annotationPostType::register_live_annotation_taxonomy');

    if (class_exists('FileDownload')) {
        add_action('init', 'FileDownload::download_file_hook');
        FileDownload::addPostTypeDataFunction(LIVE_ANNOTATION_POST_TYPE_NAME,'AnnotationData::get_study_as_csv');
    }

    if (class_exists('FieldEditor')) {

        FieldEditor::addFieldConfiguration(new FieldConfigurationLiveAnnotation());

// remove default fields we don't want
        add_action('do_meta_boxes', 'FieldEditor::remove_default_fields', 10, 3);

// custom fields specific to our post type
        add_action('do_meta_boxes', 'FieldEditor::create_meta_box'); // add custom fields
        add_action('save_post', 'FieldEditor::save_custom_fields', 1, 2); // allows saving changes that user makes
    }

// associate template with post type
    add_filter('template_include', 'associate_live_annotation_template');


//    add_action("wp_insert_post", "AnnotationData::create_study_file_callback", 10, 3);


// javascript for manipulating custom fields on the post type admin page
//function add_admin_js() {
//
//    wp_enqueue_script('admin-js',
//        plugins_url('js/admin-fields.js', __FILE__), array('jquery'));
//
//}
//add_action( 'admin_enqueue_scripts', 'add_admin_js' );


// javascript code for inclusion on the live_annotation post (as rendered by the template)
    add_action("wp_enqueue_scripts", function () {

        wp_enqueue_script('live-annotation',
            plugins_url('js/live-annotation.js', __FILE__), array('jquery'));

        wp_register_style('live-annotation-css',
            plugins_url('css/live-annotation.css', __FILE__), array(), '1.0', 'all');
        wp_enqueue_style('live-annotation-css');
    });


// add REST controller for the page's data retrieval ajax call
    add_action('rest_api_init', function () {
        $rest_controller = new RestControllerLiveAnnotation();
        $rest_controller->register_routes();
    });

// from https://gist.github.com/rmpel/e1e2452ca06ab621fe061e0fde7ae150
// enable csv uploads until issue is fixed in word press


    add_filter('wp_check_filetype_and_ext', function ($values, $file, $filename, $mimes) {
        if (extension_loaded('fileinfo')) {
            // with the php-extension, a CSV file is issues type text/plain so we fix that back to
            // text/csv by trusting the file extension.
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $real_mime = finfo_file($finfo, $file);
            finfo_close($finfo);
            if ($real_mime === 'text/plain' && preg_match('/\.(csv)$/i', $filename)) {
                $values['ext'] = 'csv';
                $values['type'] = 'text/csv';
            }
        } else {
            // without the php-extension, we probably don't have the issue at all, but just to be sure...
            if (preg_match('/\.(csv)$/i', $filename)) {
                $values['ext'] = 'csv';
                $values['type'] = 'text/csv';
            }
        }
        return $values;
    }, PHP_INT_MAX, 4);


} // initialize_plugin_live_annotation()

function tutorial_step( $atts, $content = null ) {
	$a = shortcode_atts( array(
		'class' => '',
	), $atts );

	return '<div class="step ' . esc_attr($a['class']) . '">' . $content . '</div>';
}
add_shortcode( 'step', 'tutorial_step' );


add_action('plugins_loaded', 'initialize_plugin_live_annotation');
add_action('plugins_loaded', 'AnnotationData::db_initialize'); // in case we upgraded the table
register_activation_hook(__FILE__, 'AnnotationData::db_initialize');

// Disable admin toolbar for all users except administrators on the front-end
// function wpabsolute_hide_admin_bar($content) {
//     // Returns true if user is administrator, otherwise false.
//     return ( current_user_can( 'edit_pages' ) ) ? $content : false;
// }
// add_filter( 'show_admin_bar' , 'wpabsolute_hide_admin_bar');

//Add class to body for logged in users
function my_body_classes( $classes ) {
    if (!current_user_can( 'edit_pages' )) {
        $classes[] = 'annotation-user';
    }
    return $classes;
}
add_filter( 'body_class','my_body_classes' );

?>
